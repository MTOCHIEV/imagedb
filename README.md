viewR is a Spring Boot based media management and tagging application.

![The interface of viewR](/src/main/webapp/resources/images/interface.png "Interface"){: .shadow}

* A demo version with guest permissions is available at
* http://viewr.art/

## Features

#### Sources
* Add (e.g. Google / *google.com*)
* Delete
#### Media
* Add / Download / Delete
* Media items are stored in three ways:
    * Original
    * Details Preview (600px*600px)
    * Thumbnail Preview (200px*200px)
* The full original media item is accessed through the download button and full preview when clicking the details image
* Thumbnails and detail previews are cropped to show only the middle part of the mediaItem
    * Thumbnails will be watermarked with "GIF" for gifs and "VIDEO" for video files
* Media uploads work up to 250MB
* Uploaded video files will be converted to 940px*940px h264 video (keeping aspect ratio) and aac audio
* The full preview is scaled to values assigned on the settings page (keeping aspect ratio)
* Supported formats:
    * images: jpeg, png, svg, tiff, webp, bmp, gif
    * videos: flv, mpeg, mp4, mkv, webm
#### Tags
* Add manually
* Auto-generated by Google Cloud Vision API
* Delete
#### Search
* Media filtering with AND search (all search terms have to apply)
* Search by:
    * Tag
    * Source
### Sort
* Sort media files by:
    * File Size (ascending, descending)
    * Date Added (ascending, descending)
#### Statistics
* Total file size of library
* Total amount of images in library

## Stored Media Item Details *with examples*

* **Index** *4*
* **Media UUID** *7942a25a-d0bf-4d13-85b6-4758d5675086*
* **Source** *ArtVee*
* **Source Username** *UsernameOnPlatform*
* **Added on** *2023-08-29 15:22:05.339*
* **Original Width** *1189*
* **Original Height** *1800*
* **Original Size** *1.56 MB*
* **Full Media Item Filepath** *\images\full\image_7942a25a-d0bf-4d13-85b6-4758d5675086_11178posdl.jpg*
* **Thumbnail Filepath** *\images\thumbnails\image_7942a25a-d0bf-4d13-85b6-4758d5675086_11178posdl.jpg*
* **Details Preview Filepath** *\images\details\image_7942a25a-d0bf-4d13-85b6-4758d5675086_11178posdl.jpg*
* **File Size in bits** *12480000*
* **Media Item Type** *jpeg*
* **ScaledWidth** *1560*
* **ScaledHeight** *900*
* **List of Tags** *illustration, art, comic, old*
* **Media Item is Video** *false*

## Stored Source Details *with examples*
* **Index** *1*
* **Display name** *ArtVee*
* **Website** *https://artvee.com/*
* **Added on** *2023-08-29 15:21:29.75*

## User Roles and associated rights
* Guest
    * View and download media items
    * View statistics and set their full view scale
* Editor
    * Upload media items
    * Add / Remove tags
    * Add / Remove sources
* Admin
    * See and edit user permissions
    * Admin role can not be removed from user set in env.properties

## Tech Stack
* Java *8*
* Spring Boot *2.7.15*
* Apache Tomcat *9*
* Gradle *8.3*
* Mojarra *2.3.19*
* Primefaces *12*
* PostgreSQL *14*
* Google Cloud Vision *1.2.8*
* *see more dependencies in build.gradle file*
