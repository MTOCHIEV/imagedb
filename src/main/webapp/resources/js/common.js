function pauseVid() {
    let vid = document.getElementById("mediaItemForm:currentVideo_video");
    vid.pause();
}

var isPlaying = false;

function playPauseVideo() {
    let vid = document.getElementById("mediaItemForm:currentVideo_video");
    vid.focus();
    if (isPlaying) {
        vid.pause();
    } else {
        vid.play();
    }
    isPlaying = !isPlaying;
}