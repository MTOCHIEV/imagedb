package viewR.data;

import lombok.Data;
import lombok.NoArgsConstructor;
import viewR.util.viewRUtils;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.persistence.*;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Data
@Entity
@Table(name = "users")
@NoArgsConstructor
public class User {

    @Id
    private String username;
    private byte[] passwordHash;
    private byte[] salt;
    @ElementCollection(fetch = FetchType.EAGER)
    private List<String> permissions;

    private int fullViewMaxWidth;
    private int fullViewMaxHeight;

    public User(String username, String password, String... newPermission) throws NoSuchAlgorithmException, InvalidKeySpecException {
        this.username = username;

        this.salt = new byte[16];
        viewRUtils.RANDOM.nextBytes(salt);
        KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 65536, 128);
        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        passwordHash = factory.generateSecret(spec).getEncoded();

        this.permissions = new ArrayList<>();
        for (String permission : newPermission) {
            permissions.add(permission);
        }
        this.fullViewMaxWidth = 1500;
        this.fullViewMaxHeight = 900;
    }

    public boolean matchesPassword(String passwordToCompare) throws NoSuchAlgorithmException, InvalidKeySpecException {
        KeySpec spec = new PBEKeySpec(passwordToCompare.toCharArray(), salt, 65536, 128);
        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        byte[] newPasswordHash = factory.generateSecret(spec).getEncoded();
        return Arrays.equals(newPasswordHash, this.passwordHash);
    }

    public boolean hasPermission(String permission) {
        return permissions.contains(permission);
    }

    public void setFullViewDimensions(int width, int height) {
        this.fullViewMaxWidth = width;
        this.fullViewMaxHeight = height;
    }
}
