package viewR.data;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

@Data
@Entity
@Table(name = "sources")
@NoArgsConstructor
public class Source {

    @Id
    @GeneratedValue
    private int id;
    private String displayName;
    private String website;
    private Timestamp createdOn;

    public Source(String displayName, String website) {
        this.displayName = displayName;
        this.website = website;
        this.createdOn = new Timestamp(System.currentTimeMillis());
    }
}

