package viewR.data;

import com.google.common.io.Files;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.coobird.thumbnailator.filters.Caption;
import net.coobird.thumbnailator.geometry.Position;
import net.coobird.thumbnailator.geometry.Positions;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.Java2DFrameConverter;
import org.imgscalr.Scalr;
import org.primefaces.model.file.UploadedFile;
import viewR.util.SupportedMediaType;
import viewR.util.viewRUtils;
import ws.schild.jave.Encoder;
import ws.schild.jave.EncoderException;
import ws.schild.jave.MultimediaObject;
import ws.schild.jave.encode.AudioAttributes;
import ws.schild.jave.encode.EncodingAttributes;
import ws.schild.jave.encode.VideoAttributes;
import ws.schild.jave.encode.enums.X264_PROFILE;
import ws.schild.jave.filters.ScaleFilter;

import javax.imageio.ImageIO;
import javax.persistence.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Data
@Entity
@Table(name = "mediaItems")
@NoArgsConstructor
public class MediaItem {

    private static final int THUMBNAIL_MAX_DIMENSIONS = 200;
    private static final int DETAILS_MAX_DIMENSIONS = 600;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;
    private String source;
    private String sourceUsername;
    private String mediaType;
    private String filePathFull;
    private String filePathThumbnail;
    private String filePathDetails;
    private String mediaUUID;
    private int originalWidth;
    private int originalHeight;
    private int fileSize;
    private String fileSizeAsText;
    private Timestamp addedOn;
    private boolean mediaItemIsVideo;
    @ElementCollection(fetch = FetchType.EAGER)
    private List<String> tags;

    public MediaItem(UploadedFile file, String source, String sourceUsername) throws IOException, EncoderException {
        this.mediaType = file.getContentType().substring(file.getContentType().lastIndexOf("/") + 1);
        this.mediaItemIsVideo = isVideo();
        this.addedOn = new Timestamp(System.currentTimeMillis());
        this.mediaUUID = UUID.randomUUID().toString();
        String fileExtension = FilenameUtils.getExtension(file.getFileName());
        this.filePathFull = System.getProperty("user.dir") + "\\media\\full\\" + mediaUUID + "." + fileExtension;
        this.filePathThumbnail = System.getProperty("user.dir") + "\\media\\thumbnails\\" + mediaUUID + "." + fileExtension;
        this.filePathDetails = System.getProperty("user.dir") + "\\media\\details\\" + mediaUUID + "." + fileExtension;
        this.fileSize = (int) file.getSize();
        this.fileSizeAsText = viewRUtils.convertFileSizeToText((int) file.getSize());
        this.tags = new ArrayList<>();
        this.source = source;
        this.sourceUsername = sourceUsername;
        if (mediaItemIsVideo) {
            filePathFull = FilenameUtils.removeExtension(filePathFull) + ".mp4";
            filePathThumbnail = FilenameUtils.removeExtension(filePathThumbnail) + ".png";
            filePathDetails = FilenameUtils.removeExtension(filePathDetails) + ".png";
        }

        byte[] fullMediaItem;
        byte[] thumbNail;
        byte[] detailsImage;

        if (isMediaItemIsVideo()) {
            fullMediaItem = convertVideoToMP4(file);
        } else {
            fullMediaItem = file.getContent();
        }

        writeToFileSystem(filePathFull, fullMediaItem);
        thumbNail = generateThumbnail(file);
        writeToFileSystem(filePathThumbnail, thumbNail);
        detailsImage = generateDetailsImage(file);
        writeToFileSystem(filePathDetails, detailsImage);
    }


    private byte[] generateThumbnail(UploadedFile file) throws IOException {
        BufferedImage bufferedImage = null;

        if (mediaItemIsVideo) {
            FFmpegFrameGrabber g = new FFmpegFrameGrabber(filePathFull);
            g.start();
            Java2DFrameConverter converter = new Java2DFrameConverter();
            for (int i = 0; i < g.getLengthInFrames() / 2; i++) {
                Frame frame = g.grabImage();
                bufferedImage = converter.convert(frame);
            }
            g.stop();
        } else {
            bufferedImage = ImageIO.read(new ByteArrayInputStream(file.getContent()));
        }

        this.originalWidth = bufferedImage.getWidth();
        this.originalHeight = bufferedImage.getHeight();
        int newWidth;
        int newHeight;

        if (originalWidth < originalHeight) {
            newWidth = THUMBNAIL_MAX_DIMENSIONS;
            newHeight = (int) Math.ceil(((double) newWidth * originalHeight) / originalWidth);
        } else {
            newHeight = THUMBNAIL_MAX_DIMENSIONS;
            newWidth = (int) Math.ceil(((double) newHeight * originalWidth) / originalHeight);
        }

        BufferedImage resizedImage = Scalr.resize(bufferedImage, newWidth, newHeight);
        int cropStartX = (int) Math.ceil(((double) resizedImage.getWidth() / 2) - ((double) THUMBNAIL_MAX_DIMENSIONS / 2));
        int cropStartY = (int) Math.ceil(((double) resizedImage.getHeight() / 2) - ((double) THUMBNAIL_MAX_DIMENSIONS / 2));
        BufferedImage croppedImage = Scalr.crop(resizedImage, cropStartX, cropStartY, THUMBNAIL_MAX_DIMENSIONS, THUMBNAIL_MAX_DIMENSIONS);

        if (file.getContentType().contains("gif")) {
            String caption = "GIF";
            Font font = new Font("Monospaced", Font.BOLD, 28);
            Position position = Positions.BOTTOM_RIGHT;
            Caption filter = new Caption(caption, font, Color.WHITE, position, 6);
            croppedImage = filter.apply(croppedImage);
        }

        if (mediaItemIsVideo) {
            String caption = "VIDEO";
            Font font = new Font("Monospaced", Font.BOLD, 28);
            Position position = Positions.BOTTOM_RIGHT;
            Caption filter = new Caption(caption, font, Color.WHITE, position, 6);
            croppedImage = filter.apply(croppedImage);
        }

        String outputMediaType = mediaType;
        if (mediaItemIsVideo) outputMediaType = "png";
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(croppedImage, outputMediaType, baos);
        return baos.toByteArray();
    }

    private byte[] generateDetailsImage(UploadedFile file) throws IOException {
        BufferedImage bufferedImage = null;

        if (file.getContentType().contains("video")) {
            FFmpegFrameGrabber g = new FFmpegFrameGrabber(filePathFull);
            g.start();
            Java2DFrameConverter converter = new Java2DFrameConverter();
            for (int i = 0; i < g.getLengthInFrames() / 2; i++) {
                Frame frame = g.grabImage();
                bufferedImage = converter.convert(frame);
            }
            g.stop();
        } else {
            bufferedImage = ImageIO.read(new ByteArrayInputStream(file.getContent()));
        }

        int newWidth;
        int newHeight;

        if (originalWidth < originalHeight) {
            newWidth = DETAILS_MAX_DIMENSIONS;
            newHeight = (int) Math.ceil(((double) newWidth * originalHeight) / originalWidth);
        } else {
            newHeight = DETAILS_MAX_DIMENSIONS;
            newWidth = (int) Math.ceil(((double) newHeight * originalWidth) / originalHeight);
        }

        BufferedImage resizedImage = Scalr.resize(bufferedImage, newWidth, newHeight);
        int cropStartX = (int) Math.ceil(((double) resizedImage.getWidth() / 2) - ((double) DETAILS_MAX_DIMENSIONS / 2));
        int cropStartY = (int) Math.ceil(((double) resizedImage.getHeight() / 2) - ((double) DETAILS_MAX_DIMENSIONS / 2));
        BufferedImage croppedImage = Scalr.crop(resizedImage, cropStartX, cropStartY, DETAILS_MAX_DIMENSIONS, DETAILS_MAX_DIMENSIONS);

        String outputMediaType = mediaType;
        if (mediaItemIsVideo) outputMediaType = "png";
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(croppedImage, outputMediaType, baos);
        return baos.toByteArray();
    }

    private void writeToFileSystem(String filePath, byte[] content) throws IOException {
        Files.write(content, new File(filePath));
    }

    private boolean isVideo() {
        List videoMediaTypes = Arrays.stream(SupportedMediaType.values()).filter(e -> e.getMediaType().contains("video")).collect(Collectors.toList());
        return videoMediaTypes.stream().anyMatch(o -> mediaType.equalsIgnoreCase(o.toString()));
    }

    private byte[] convertVideoToMP4(UploadedFile file) throws IOException, EncoderException {
        File sourceFile = File.createTempFile("tmp", "source");
        FileOutputStream fileOutputStream = new FileOutputStream(sourceFile);
        fileOutputStream.write(file.getContent());
        File targetFile = File.createTempFile("tmp", "target");
        AudioAttributes audio = new AudioAttributes();
        audio.setCodec("aac");
        audio.setBitRate(64000);
        audio.setChannels(2);
        audio.setSamplingRate(44100);
        VideoAttributes video = new VideoAttributes();
        video.setCodec("h264");
        video.setX264Profile(X264_PROFILE.HIGH);
        video.setBitRate(500000);
        video.addFilter(new ScaleFilter("if(gte(iw\\,ih)\\,min(940\\,iw)\\,-2):if(lt(iw\\,ih)\\,min(940\\,ih)\\,-2)"));
        EncodingAttributes attrs = new EncodingAttributes();
        attrs.setOutputFormat("mp4");
        attrs.setAudioAttributes(audio);
        attrs.setVideoAttributes(video);
        Encoder encoder = new Encoder();
        encoder.encode(new MultimediaObject(sourceFile), targetFile, attrs);
        return FileUtils.readFileToByteArray(targetFile);
    }
}
