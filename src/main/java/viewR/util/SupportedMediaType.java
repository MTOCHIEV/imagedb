package viewR.util;

public enum SupportedMediaType {

    //Images
    jpe("jpe", "image/jpeg"),
    jpeg("jpeg", "image/jpeg"),
    jpg("jpg", "image/jpeg"),
    png("png", "image/png"),
    svg("svg", "image/svg+xml"),
    tif("tif", "image/tiff"),
    tiff("tiff", "image/tiff"),
    webp("webp", "image/webp"),
    bmp("bmp", "image/bmp"),
    gif("gif","image/gif"),

    //Videos
    flv("flv", "video/x-flv"),
    m1v("m1v", "video/mpeg"),
    m2v("m2v", "video/mpeg"),
    m4v("m4v", "video/mp4"),
    mk3d("mk3d", "video/x-matroska"),
    mks("mks", "video/x-matroska"),
    mkv("mkv", "video/x-matroska"),
    mp4("mp4", "video/mp4"),
    mp4v("mp4v", "video/mp4"),
    mpe("mpe", "video/mpeg"),
    mpeg("mpeg", "video/mpeg"),
    mpg("mpg", "video/mpeg"),
    mpg4("mpg4", "video/mp4"),
    webm("webm", "video/webm");

    private final String fileExtension;
    private final String mediaType;

    SupportedMediaType (String fileExtension, String mediaType) {
        this.fileExtension = fileExtension;
        this.mediaType = mediaType;
    }

    public String getFileExtension() {
        return fileExtension;
    }

    public String getMediaType() {
        return mediaType;
    }
}
