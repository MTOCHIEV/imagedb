package viewR.util;

import org.springframework.stereotype.Component;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.security.SecureRandom;
import java.util.Random;

@Component
public class viewRUtils {

    public static final Random RANDOM = new SecureRandom();

    public static void addFacesMessage(FacesMessage.Severity type, String summary, String details) {
        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
        FacesMessage message = new FacesMessage(type, summary, details);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public static String convertFileSizeToText(int fileSizeInByte) {
        long kilo = 1024;
        long mega = kilo * kilo;
        long giga = mega * kilo;
        long tera = giga * kilo;

        String s = "";
        double kb = (double) fileSizeInByte / kilo;
        double mb = kb / kilo;
        double gb = mb / kilo;
        double tb = gb / kilo;
        if (fileSizeInByte < kilo) {
            s = fileSizeInByte + " Bytes";
        } else if (fileSizeInByte >= kilo && fileSizeInByte < mega) {
            s = String.format("%.2f", kb) + " KB";
        } else if (fileSizeInByte >= mega && fileSizeInByte < giga) {
            s = String.format("%.2f", mb) + " MB";
        } else if (fileSizeInByte >= giga && fileSizeInByte < tera) {
            s = String.format("%.2f", gb) + " GB";
        } else if (fileSizeInByte >= tera) {
            s = String.format("%.2f", tb) + " TB";
        }
        return s;
    }

}
