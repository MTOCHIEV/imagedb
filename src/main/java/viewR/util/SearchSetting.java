package viewR.util;

public enum SearchSetting {
    FILE_SIZE_ASC,
    FILE_SIZE_DESC,
    DATE_ASC,
    DATE_DESC;
}
