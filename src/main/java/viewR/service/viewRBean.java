package viewR.service;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.PrimeFaces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import viewR.api.MediaRepository;
import viewR.api.SourceRepository;
import viewR.data.MediaItem;
import viewR.data.Source;
import viewR.util.SearchSetting;
import viewR.util.viewRUtils;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

@Data
@Named("viewRBean")
@ViewScoped
public class viewRBean {

    @Autowired
    private MediaRepository mediaRepository;
    private List<Integer> mediaItemIds;
    private MediaItem loadedMediaItem;
    @Autowired
    private SourceRepository sourceRepository;
    private boolean detailsVisible = false;
    private int currentMediaItemID;
    private List<String> searchEntries;
    private String newTag;

    @Value("${viewR.version}")
    private String viewRVersion;

    public void redirectToHome() throws IOException {
        String contextPath = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();
        String mainURL = contextPath + "/viewR.xhtml";
        FacesContext.getCurrentInstance().getExternalContext().redirect(mainURL);
    }

    public void toggleDetails(String newMediaItemID) {
        if (!detailsVisible) {
            currentMediaItemID = 0;
            clearMediaItem();
        }
        if (currentMediaItemID == 0 && !detailsVisible) {
            detailsVisible = !detailsVisible;
        } else if (Integer.parseInt(newMediaItemID) == currentMediaItemID) {
            detailsVisible = !detailsVisible;
        }
        currentMediaItemID = Integer.parseInt(newMediaItemID);

        loadMediaItem(currentMediaItemID);
    }

    public void loadMediaItem(int newMediaItemID) {
        loadedMediaItem = mediaRepository.findById(newMediaItemID).orElse(null);
    }

    private void clearMediaItem() {
        newTag = null;
        detailsVisible = false;
        currentMediaItemID = 0;
        loadedMediaItem = null;
    }

    public void deleteMediaItem() {
        try {
            Files.delete(Paths.get(loadedMediaItem.getFilePathThumbnail()));
            Files.delete(Paths.get(loadedMediaItem.getFilePathDetails()));
            Files.delete(Paths.get(loadedMediaItem.getFilePathFull()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        mediaRepository.deleteById(loadedMediaItem.getId());
        mediaItemIds.remove((Integer) currentMediaItemID);
        clearMediaItem();

        viewRUtils.addFacesMessage(FacesMessage.SEVERITY_INFO, "SUCCESS", "Media Item Deleted");
    }

    public List<String> completeSearch(String query) {
        List<String> searchSuggestions = new ArrayList<>();
        List<String> sourceNames = new ArrayList<>();
        List<String> listOfAllTags = new ArrayList<>();

        for (Source source : sourceRepository.findAll()) {
            sourceNames.add(source.getDisplayName().toUpperCase());
        }
        for (MediaItem mediaItem : mediaRepository.findAll()) {
            listOfAllTags.addAll(mediaItem.getTags());
        }

        searchSuggestions.addAll(sourceNames);
        searchSuggestions.addAll(listOfAllTags);
        searchSuggestions = new ArrayList<String>(new HashSet<String>(searchSuggestions));

        return searchSuggestions.stream().sorted().filter(t -> StringUtils.containsIgnoreCase(t, query)).collect(Collectors.toList());
    }

    public void deleteTag(String tag) {
        loadedMediaItem.getTags().remove(tag);
        mediaRepository.save(loadedMediaItem);

        FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("mediaItemID", currentMediaItemID);

        viewRUtils.addFacesMessage(FacesMessage.SEVERITY_INFO, "SUCCESS", "Tag Deleted");
    }

    public void addTag() {
        loadedMediaItem.getTags().add(newTag);
        mediaRepository.save(loadedMediaItem);
        FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("mediaItemID", currentMediaItemID);
        viewRUtils.addFacesMessage(FacesMessage.SEVERITY_INFO, "SUCCESS", "Tag Added");
    }

    @PostConstruct
    public void init() {
        updateDB();
    }

    public void updateDB() {
        updateAllMediaItems();
    }

    public void updateAllMediaItems() {
        mediaItemIds = new ArrayList<>();
        for (MediaItem mediaItem : mediaRepository.findAll()) {
            mediaItemIds.add(mediaItem.getId());
        }
        Collections.sort(mediaItemIds);
    }

    public void filterSearch() {
        clearMediaItem();

        mediaItemIds = new ArrayList<>();
        for (MediaItem mediaItem : mediaRepository.findAll()) {
            mediaItemIds.add(mediaItem.getId());

            for (String s : searchEntries) {
                if (!StringUtils.containsIgnoreCase(StringUtils.join(mediaItem.getTags(), ","), s)) {
                    if (!StringUtils.containsIgnoreCase(mediaItem.getSource(), s)) {
                        mediaItemIds.remove((Integer) mediaItem.getId());
                    }
                }
            }
        }
        Collections.sort(mediaItemIds);
    }

    public void sortSearch(SearchSetting searchSetting) {
        clearMediaItem();

        List<MediaItem> mediaItemList = new ArrayList<>();
        for (int mediaItemID : mediaItemIds) mediaItemList.add(mediaRepository.findById(mediaItemID).orElse(null));
        if (searchSetting.equals(SearchSetting.DATE_ASC)) {
            Collections.sort(mediaItemList, (Comparator.comparing(MediaItem::getAddedOn)));
        } else if (searchSetting.equals(SearchSetting.DATE_DESC)) {
            Collections.sort(mediaItemList, (Comparator.comparing(MediaItem::getAddedOn)).reversed());
        } else if (searchSetting.equals(SearchSetting.FILE_SIZE_ASC)) {
            Collections.sort(mediaItemList, (Comparator.comparing(MediaItem::getFileSize)));
        } else if (searchSetting.equals(SearchSetting.FILE_SIZE_DESC)) {
            Collections.sort(mediaItemList, (Comparator.comparing(MediaItem::getFileSize)).reversed());
        }
        mediaItemIds = new ArrayList<>();
        for (MediaItem mediaItem : mediaItemList) mediaItemIds.add(mediaItem.getId());
    }

    public void searchTag(String tag) {
        searchEntries = new ArrayList<>();
        searchEntries.add(tag);
        filterSearch();
    }

    public void nextMediaItem() {
        if (loadedMediaItem != null) {
            int currentMediaItemIDIndex = mediaItemIds.indexOf(currentMediaItemID);
            int nextMediaItemIDIndex = currentMediaItemIDIndex;
            if (mediaItemIds.indexOf(currentMediaItemID) + 1 < mediaItemIds.size()) {
                nextMediaItemIDIndex = mediaItemIds.indexOf(currentMediaItemID) + 1;
            }
            currentMediaItemID = mediaItemIds.get(nextMediaItemIDIndex);
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("mediaItemID", String.valueOf(currentMediaItemID));
            loadMediaItem(currentMediaItemID);
            PrimeFaces.current().executeScript("PF('fullMediaItemDialog').show()");
        }
    }

    public void previousMediaItem() {
        if (loadedMediaItem != null) {
            int currentMediaItemIDIndex = mediaItemIds.indexOf(currentMediaItemID);
            int previousMediaItemIDIndex = currentMediaItemIDIndex;
            if (mediaItemIds.indexOf(currentMediaItemID) - 1 >= 0) {
                previousMediaItemIDIndex = mediaItemIds.indexOf(currentMediaItemID) - 1;
            }
            currentMediaItemID = mediaItemIds.get(previousMediaItemIDIndex);
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("mediaItemID", String.valueOf(currentMediaItemID));
            loadMediaItem(currentMediaItemID);
            PrimeFaces.current().executeScript("PF('fullMediaItemDialog').show()");
        }
    }

    public String getVersion() {
        return viewRVersion;
    }
}