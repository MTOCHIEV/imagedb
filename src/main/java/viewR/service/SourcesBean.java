package viewR.service;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import viewR.data.Source;
import viewR.api.SourceRepository;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

@Data
@Named("sourcesBean")
@RequestScoped
public class SourcesBean {

    @Autowired
    private SourceRepository sourceRepository;

    private List<String> sourceNames;

    @PostConstruct
    public void init() {
        updateDB();
    }

    public void updateDB() {
        updateAllSources();
    }

    public void updateAllSources() {
        sourceNames = new ArrayList<>();
        for (Source source : sourceRepository.findAll()) {
            sourceNames.add(source.getDisplayName());
        }
    }

}
