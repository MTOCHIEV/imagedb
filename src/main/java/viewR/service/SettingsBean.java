package viewR.service;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import viewR.api.MediaRepository;
import viewR.api.SourceRepository;
import viewR.api.UserRepository;
import viewR.data.MediaItem;
import viewR.data.Source;
import viewR.data.User;
import viewR.util.viewRUtils;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

@Data
@Named("settingsBean")
@RequestScoped
public class SettingsBean {

    @Autowired
    private SourceRepository sourceRepository;
    @Autowired
    private MediaRepository mediaRepository;
    @Autowired
    private UserRepository userRepository;

    private List<Source> sources;
    private List<String> sourceNames;
    private String selectedTab;
    private String newSourceDisplayName;
    private String newSourceWebsite;
    private String nrOfImages;
    private String totalFileSize;
    private int fullViewMaxWidth;
    private int fullViewMaxHeight;

    @PostConstruct
    public void init() {
        updateAllSources();
        updateAllImages();
        getDefaultViewDimensions();
    }

    public void addNewSource() {
        if (newSourceDisplayName != null && newSourceWebsite != null) {
            Source source = new Source(newSourceDisplayName, newSourceWebsite);
            sourceRepository.save(source);
        } else {
            viewRUtils.addFacesMessage(FacesMessage.SEVERITY_ERROR, "FAILED", "new source name or website is empty");
        }
    }

    public void deleteSelectedSource(Source source) {
        sourceRepository.delete(source);
    }

    public void updateAllSources() {
        sources = new ArrayList<>();
        sourceNames = new ArrayList<>();
        for (Source source : sourceRepository.findAll()) {
            sources.add(source);
            sourceNames.add(source.getDisplayName());
        }
    }

    public void updateAllImages() {
        int imageCounter = 0;
        int fileSizeCounter = 0;
        for (MediaItem mediaItem : mediaRepository.findAll()) {
            imageCounter++;
            fileSizeCounter += mediaItem.getFileSize();
        }

        nrOfImages = String.valueOf(imageCounter);
        totalFileSize = viewRUtils.convertFileSizeToText(fileSizeCounter);
    }

    public void getDefaultViewDimensions() {
        String userID = UserBean.getLoggedInUser();
        User currentUser = userRepository.findById(userID).orElse(null);
        if (currentUser != null) {
            this.fullViewMaxWidth = currentUser.getFullViewMaxWidth();
            this.fullViewMaxHeight = currentUser.getFullViewMaxHeight();
        } else {
            this.fullViewMaxWidth = 1500;
            this.fullViewMaxHeight = 900;
        }
    }

    public void setFullViewDimensions(int maxWidth, int maxHeight) {
        String userID = UserBean.getLoggedInUser();
        User currentUser = userRepository.findById(userID).orElse(null);
        if (currentUser != null) {
            currentUser.setFullViewDimensions(maxWidth, maxHeight);
            userRepository.save(currentUser);
        }
    }

    private int calculateScaledMaxWidth(String originalWidth, String originalHeight) {
        int newWidth;
        int newHeight;
        int fullImageWidthInt = this.fullViewMaxWidth;
        int fullImageHeightInt = this.fullViewMaxHeight;
        int originalWidthInt = Integer.parseInt(originalWidth);
        int originalHeightInt = Integer.parseInt(originalHeight);
        if (originalWidthInt > originalHeightInt) {
            newWidth = fullImageWidthInt;
            newHeight = (int) Math.ceil(((double) newWidth * originalHeightInt) / originalWidthInt);
        } else {
            newHeight = fullImageHeightInt;
            newWidth = (int) Math.ceil(((double) newHeight * originalWidthInt) / originalHeightInt);
        }
        if (newHeight > fullImageHeightInt) {
            newHeight = fullImageHeightInt;
            newWidth = (int) Math.ceil(((double) newHeight * originalWidthInt) / originalHeightInt);
        }
        if (newWidth > fullImageWidthInt) {
            newWidth = fullImageWidthInt;
            newHeight = (int) Math.ceil(((double) newWidth * originalHeightInt) / originalWidthInt);
        }
        return newWidth;
    }

    private int calculateScaledMaxHeight(String originalWidth, String originalHeight) {
        int newWidth;
        int newHeight;
        int fullImageWidthInt = this.fullViewMaxWidth;
        int fullImageHeightInt = this.fullViewMaxHeight;
        int originalWidthInt = Integer.parseInt(originalWidth);
        int originalHeightInt = Integer.parseInt(originalHeight);
        if (originalWidthInt > originalHeightInt) {
            newWidth = fullImageWidthInt;
            newHeight = (int) Math.ceil(((double) newWidth * originalHeightInt) / originalWidthInt);
        } else {
            newHeight = fullImageHeightInt;
            newWidth = (int) Math.ceil(((double) newHeight * originalWidthInt) / originalHeightInt);
        }
        if (newHeight > fullImageHeightInt) {
            newHeight = fullImageHeightInt;
            newWidth = (int) Math.ceil(((double) newHeight * originalWidthInt) / originalHeightInt);
        }
        if (newWidth > fullImageWidthInt) {
            newWidth = fullImageWidthInt;
            newHeight = (int) Math.ceil(((double) newWidth * originalHeightInt) / originalWidthInt);
        }
        return newHeight;
    }

    public int getFullViewMaxScaledWidth(String imageWidth, String imageHeight) {
        return calculateScaledMaxWidth(imageWidth, imageHeight);
    }

    public int getFullViewMaxScaledHeight(String imageWidth, String imageHeight) {
        return calculateScaledMaxHeight(imageWidth, imageHeight);
    }
}
