package viewR.service;

import lombok.Data;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import viewR.data.MediaItem;
import viewR.api.MediaRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import javax.inject.Named;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

@Data
@Named("mediaItemStreamerBean")
@ApplicationScoped
public class MediaItemStreamer {

    @Autowired
    private MediaRepository mediaRepository;

    public StreamedContent getThumbnail() throws IOException {
        FacesContext context = FacesContext.getCurrentInstance();

        int mediaItemID = Integer.parseInt(context.getExternalContext().getRequestParameterMap().get("mediaItemID"));

        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            return new DefaultStreamedContent();
        } else {
            MediaItem loadedMediaItem = mediaRepository.findById(mediaItemID).orElse(null);
            byte[] mediaItemAsBytes = Files.readAllBytes(Paths.get(loadedMediaItem.getFilePathThumbnail()));
            return DefaultStreamedContent.builder().contentType(loadedMediaItem.getMediaType()).name(loadedMediaItem.getMediaUUID()).stream(() -> new ByteArrayInputStream(mediaItemAsBytes)).build();
        }
    }

    public StreamedContent getDetailsImage() throws IOException {
        FacesContext context = FacesContext.getCurrentInstance();

        int mediaItemID = Integer.parseInt(context.getExternalContext().getRequestParameterMap().get("mediaItemID"));

        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            return new DefaultStreamedContent();
        } else {
            MediaItem loadedMediaItem = mediaRepository.findById(mediaItemID).orElse(null);
            byte[] mediaItemAsBytes = Files.readAllBytes(Paths.get(loadedMediaItem.getFilePathDetails()));
            return DefaultStreamedContent.builder().contentType(loadedMediaItem.getMediaType()).name(loadedMediaItem.getMediaUUID()).stream(() -> new ByteArrayInputStream(mediaItemAsBytes)).build();
        }
    }

    public StreamedContent getFullMediaItem() throws IOException {
        FacesContext context = FacesContext.getCurrentInstance();

        int mediaItemID = Integer.parseInt(context.getExternalContext().getRequestParameterMap().get("mediaItemID"));

        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            return new DefaultStreamedContent();
        } else {
            MediaItem loadedMediaItem = mediaRepository.findById(mediaItemID).orElse(null);
            byte[] mediaItemAsBytes = Files.readAllBytes(Paths.get(loadedMediaItem.getFilePathFull()));
            return DefaultStreamedContent.builder().contentType(loadedMediaItem.getMediaType()).name(loadedMediaItem.getMediaUUID()).stream(() -> new ByteArrayInputStream(mediaItemAsBytes)).build();
        }
    }

    public void downloadFullMediaItem() throws IOException {
        FacesContext context = FacesContext.getCurrentInstance();
        int mediaItemID = Integer.parseInt(context.getExternalContext().getRequestParameterMap().get("mediaItemID"));
        MediaItem loadedMediaItem = mediaRepository.findById(mediaItemID).orElse(null);

        File file = new File(loadedMediaItem.getFilePathFull());
        String fileName = file.getName();
        String contentType = context.getExternalContext().getMimeType(fileName);
        int contentLength = (int) file.length();
        context.getExternalContext().responseReset();
        context.getExternalContext().setResponseContentType(contentType);
        context.getExternalContext().setResponseContentLength(contentLength);
        context.getExternalContext().setResponseHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
        OutputStream output = context.getExternalContext().getResponseOutputStream();
        Files.copy(file.toPath(), output);
        context.responseComplete();
    }

}

