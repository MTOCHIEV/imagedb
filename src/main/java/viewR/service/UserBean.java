package viewR.service;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import viewR.api.UserRepository;
import viewR.data.User;
import viewR.util.viewRUtils;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

@Data
@Named("userBean")
@SessionScoped
public class UserBean {

    @Autowired
    UserRepository userRepository;

    @Value("${admin.username}")
    private String adminUsername;


    private String selectedUserID;
    private String selectedPermission;
    private String newPermission;
    private User loadedUser;
    private boolean userLoaded;

    private String newUserName;
    private String newUserPassword;
    private String newUserPassword2;

    public static String getLoggedInUser() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        return session.getAttribute("username").toString();
    }

    public void loadUser(String username) {
        String userID = username;
        User user = userRepository.findById(userID).orElse(null);
        if (user != null) {
            this.userLoaded = true;
            this.loadedUser = user;
        } else {
            this.userLoaded = false;
        }
    }

    public boolean hasPermission(String permission) {
        String userID = getLoggedInUser();
        User user = userRepository.findById(userID).orElse(null);
        if (user != null) {
            return user.hasPermission(permission);
        }
        return false;
    }

    public void addPermission(String username, String permission) {
        String userID = username;
        User user = userRepository.findById(userID).orElse(null);
        if (user != null) {
            user.getPermissions().add(permission);
            userRepository.save(user);
        }
    }

    public void removePermission(String username, String permission) {
        if (username.equalsIgnoreCase(adminUsername) && permission.equalsIgnoreCase("admin")) {
            System.out.println("WARNING: Can not remove admin permission from original admin user");
            return;
        } else {
            String userID = username;
            User user = userRepository.findById(userID).orElse(null);
            if (user != null) {
                user.getPermissions().remove(permission);
                userRepository.save(user);
            }
        }
    }

    public void createNewUser() throws NoSuchAlgorithmException, InvalidKeySpecException, IOException {
        String contextPath = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();
        String loginURL = contextPath + "/login.xhtml";
        User checkExisting = userRepository.findById(this.newUserName).orElse(null);
        if (checkExisting != null) {
            viewRUtils.addFacesMessage(FacesMessage.SEVERITY_ERROR, "FAILED!", "Username already exists");
        } else if (!this.newUserPassword.equalsIgnoreCase(this.newUserPassword2)) {
            viewRUtils.addFacesMessage(FacesMessage.SEVERITY_ERROR, "FAILED!", "Passwords were not the same");
        } else {
            User newUser = new User(this.newUserName, this.newUserPassword, "guest");
            userRepository.save(newUser);
            viewRUtils.addFacesMessage(FacesMessage.SEVERITY_INFO, "SUCCESS!", "User successfully created");
            FacesContext.getCurrentInstance().getExternalContext().redirect(loginURL);
        }
    }
}
