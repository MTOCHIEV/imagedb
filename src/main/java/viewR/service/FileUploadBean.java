package viewR.service;

import com.google.cloud.vision.v1.AnnotateImageResponse;
import com.google.cloud.vision.v1.Feature;
import com.google.cloud.vision.v1.Likelihood;
import lombok.Data;
import org.primefaces.event.FileUploadEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gcp.vision.CloudVisionTemplate;
import org.springframework.core.io.ResourceLoader;
import viewR.api.MediaRepository;
import viewR.data.MediaItem;
import viewR.util.SupportedMediaType;
import viewR.util.viewRUtils;
import ws.schild.jave.EncoderException;

import javax.faces.application.FacesMessage;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Data
@Named("fileUploadBean")
@ViewScoped
public class FileUploadBean {

    @Autowired
    private CloudVisionTemplate cloudVisionTemplate;

    @Autowired
    private MediaRepository mediaRepository;

    @Autowired
    private ResourceLoader resourceLoader;

    private String selectedSource;
    private String selectedSourceUsername;

    public void handleFileUpload(FileUploadEvent event) throws IOException, EncoderException {
        MediaItem mediaItem = new MediaItem(event.getFile(), selectedSource, selectedSourceUsername);

        for (String s : getImageTagsGoogleCloudVision(mediaItem.getFilePathDetails())) {
            mediaItem.getTags().add(s);
        }
        mediaRepository.save(mediaItem);

        viewRUtils.addFacesMessage(FacesMessage.SEVERITY_INFO, "Success!", "The files have been uploaded");
    }

    public List<String> getImageTagsGoogleCloudVision(String filePath) {
        AnnotateImageResponse response = this.cloudVisionTemplate.analyzeImage(this.resourceLoader.getResource("file:" + filePath), Feature.Type.LABEL_DETECTION, Feature.Type.LANDMARK_DETECTION, Feature.Type.LOGO_DETECTION, Feature.Type.OBJECT_LOCALIZATION, Feature.Type.SAFE_SEARCH_DETECTION);

        List<String> descriptions = new ArrayList<>();
        response.getLabelAnnotationsList().stream().forEach(entityAnnotation -> descriptions.add(entityAnnotation.getDescription()));
        response.getLandmarkAnnotationsList().stream().forEach(entityAnnotation -> descriptions.add(entityAnnotation.getDescription()));
        response.getLogoAnnotationsList().stream().forEach(entityAnnotation -> descriptions.add(entityAnnotation.getDescription()));
        response.getLocalizedObjectAnnotationsList().stream().forEach(entityAnnotation -> descriptions.add(entityAnnotation.getName()));

        if (response.getSafeSearchAnnotation().getSpoof() == Likelihood.LIKELY || response.getSafeSearchAnnotation().getSpoof() == Likelihood.VERY_LIKELY) {
            descriptions.add("Meme");
        }
        List<String> descriptionsUnique = new ArrayList<String>(new HashSet<String>(descriptions));
        return descriptionsUnique;
    }

    public String getValidFormats() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("/(\\.|\\/)(");
        for (SupportedMediaType type : SupportedMediaType.values()) {
            stringBuilder.append(type.getFileExtension());
            stringBuilder.append("|");
        }
        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        stringBuilder.append(")$/");
        return stringBuilder.toString();
    }
}
