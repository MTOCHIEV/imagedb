package viewR.service;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import viewR.api.UserRepository;
import viewR.data.User;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

@Data
@Named("loginBean")
@SessionScoped
public class LoginBean {

    private String username;
    private String password;
    private User user;

    @Value("${admin.username}")
    private String adminUsername;
    @Value("${admin.password}")
    private String adminPassword;

    @Autowired
    private UserRepository userRepository;

    @PostConstruct
    private void init() throws NoSuchAlgorithmException, InvalidKeySpecException {
        User admin = new User(adminUsername, adminPassword, "admin", "editor", "guest");
        userRepository.save(admin);
    }

    private void getUser(String username) {
        this.user = userRepository.findById(username).orElse(null);
    }

    public void login() throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        String contextPath = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();
        String loginURLError = contextPath + "/login.xhtml?error=true";
        String mainURL = contextPath + "/viewR.xhtml";

        getUser(username);
        if (user == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect(loginURLError);
        } else {
            if (user.matchesPassword(password)) {
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("username", username);
                FacesContext.getCurrentInstance().getExternalContext().redirect(mainURL);
            } else {
                FacesContext.getCurrentInstance().getExternalContext().redirect(loginURLError);
            }
        }
    }

    public void logout() throws IOException {
        String contextPath = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();
        String loginURL = contextPath + "/login.xhtml";
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        session.invalidate();
        FacesContext.getCurrentInstance().getExternalContext().redirect(loginURL);
    }

    public void redirectToRegistration() throws IOException {
        String contextPath = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();
        String registerURL = contextPath + "/register.xhtml";
        FacesContext.getCurrentInstance().getExternalContext().redirect(registerURL);
    }
}
