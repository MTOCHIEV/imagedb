package viewR.api;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import viewR.data.User;

@Repository
public interface UserRepository extends CrudRepository<User, String> {
}