package viewR.api;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import viewR.data.MediaItem;

@Repository
public interface MediaRepository extends CrudRepository<MediaItem, Integer> {
}