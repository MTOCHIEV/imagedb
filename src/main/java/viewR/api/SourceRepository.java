package viewR.api;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import viewR.data.Source;

@Repository
public interface SourceRepository extends CrudRepository<Source, Integer> {
}